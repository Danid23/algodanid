<?php
/**
 * Created by PhpStorm.
 * User: Danid SAID
 * Date: 09/04/2019
 * Time: 08:55
 */

?>

<script>

    var tree = {
        element: 21,
        left: {
            element: 9,
            left: {
                element: 5,
                left: {
                    element: 1,
                    left: null,
                    right: null,
                },
                right: {
                    element: 8,
                    left: null,
                    right: null,
                },
            },
            right: {
                element: 12,
                left: null,
                right: null,
            }
        },
        right: {
            element: 34,
            left: {
                element: 25,
                left: null,
                right: null,
            },
            right: {
                element: 36,
                left: null,
                right: {
                    element: 35,
                    left: null,
                    right: null,
                },
            }
        }
    };
    console.dir(tree);
    //console.log(insertion(tree, tree, 3));

    function insertion(tree, node, element){
        if(element > node.element){
            if(node.right === null){
                node.right = {
                    element: element,
                    right: null,
                    left: null
                }
            }
            else{
                insertion(tree, node.right, element);
            }
        }
        else if(element < node.element){
            if(node.left === null){
                node.left = {
                    element: element,
                    right: null,
                    left: null
                }
            }
            else{
                insertion(tree, node.left, element);
            }
        }
        return node;
    }

    //console.log(postFIx(tree, tree));

    console.log(tree);
    console.log(postFIx(tree));
    console.log(preFix(tree));
    console.log(inFix(tree));

    function postFIx(node, list = []){
        if(typeof node != "undefined" || node != null){
            if(node !== null){
                if(node.left !== null){
                    postFIx(node.left, list);
                }

                if(node.right !== null){
                    postFIx(node.right, list);
                }
            }
            list.push(node.element);
        }
        return list;
    }

    function preFix(node, list = []){
        if(typeof node != "undefined" || node != null) {
            if (node !== null) {
                list.push(node.element);
                if(node.left !== null){
                    preFix(node.left, list)
                }
                if(node.right !== null){
                    preFix(node.right, list)
                }
            }
        }
        return list;
    }

    function inFix(node, list = []){
        if(typeof node != "undefined" || node != null) {
            if (node !== null) {
                if(node.left !== null){
                    inFix(node.left, list)
                }

                list.push(node.element);

                if(node.right !== null){
                    inFix(node.right, list)
                }
            }
        }
        return list;
    }
</script>
